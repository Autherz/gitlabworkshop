const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema({
  firstname: String,
  lastname: String,
  age: Number,
  createdatetime: Date
})

User.index({ 'firstname': 'text', 'lastname': 'text' }, { weights: { firstname: 2, lastname: 1 } })
// User.index({ '$**': 'text' })
module.exports = mongoose.model('Users', User)
