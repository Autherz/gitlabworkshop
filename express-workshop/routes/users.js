var express = require('express')
var router = express.Router()
const User = require('../model/user')


/* POST users listing. */
router.post('/', function (req, res, next) {

  let newuser = new User({
    firstname: 'atisit',
    lastname: 'thongbai',
    age: 22,
    createdatetime: new Date()
  })

  let person = [
    { id: '1', name: 'atisit', lname: 'thongbai' },
    { id: '2', name: 'warophen' },
    { id: '3', name: 'jjjj' }
  ]
  newuser.save()
  res.send({ message: 'createsuccess' })
})

router.get('/users', async (req, res, next) => {
  try {
    // if (req.headers.token) {
    //   res.send(await User.find())
    // } else {
    //   let e = new Error('have not token')
    //   e.status = 401
    //   throw e
    // }
    // User.find().then(docs => {
    //   res.send(docs)
    // })

    // res.send(await User.find())
    // regular expression
    // res.send(await User.find({ 'firstname': { $regex: /tisit/, $options: 'i' } }))
    // eslint-disable-next-line standard/object-curly-even-spacing
    // eslint-disable-next-line handle-callback-err
    res.send(await User.find({
      $or: [
        { firstname: new RegExp('EIEI', 'i') },
        { lastname: new RegExp('EIEI', 'i') }
      ]
    }))
    // res.send(users)
  } catch (error) {
    next(error)
  }
})

router.get('/user/:id', async (req, res, next) => {
  try {
    // eslint-disable-next-line standard/object-curly-even-spacing
    // res.send(await User.findOne({ _id: req.params.id }))
    res.send(await User.findById({ _id: req.params.id }))
    // res.send(await User.findById({ _id: req.body.search }, 'firstname'))
    // res.send(await User.findById({ name: req.params.id }, 'firstname'))
  } catch (error) {
    next(error)
  }
})

router.put('/user/:id', async (req, res, next) => {
  try {
    let user = await User.findOne({ _id: req.params.id })
    if (req.body.age) {
      user.age = req.body.age
    }
    user.save()
    res.send('update success!')
  } catch (error) {
    next(error)
  }
})

router.put('/users', async (req, res, next) => {
  try {
    let users = await User.find()
    for (const user of users) {
      user.age = req.body.age
      await user.save()
    }
    res.send('update sucess')
  } catch (error) {

  }
})

router.put('/test', async (req, res, next) => {
  try {
    var temp = req.body.id + ' ' + req.body.token
    res.send(temp)
  } catch (error) {
    next(error)
  }
})

router.delete('/user/:id', async (req, res, next) => {
  try {
    await User.findByIdAndDelete({ _id: req.params.id })
    res.send({ 'message': 'delete success' })
  } catch (error) {
    next(error)
  }
})

router.delete('/user/:id', async (req, res, next) => {
  try {
    let user = await User.findOne({ _id: req.params.id })
    await user.delete()
    res.send('delete success')
  } catch (error) {
    next(error)
  }
})

module.exports = router
