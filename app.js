
const hello = require('./hello');

hello.fnHello()
console.log(hello.foo)

let number = 'xxxxxxx \''

console.log(number)

// ====================
const greeting = (person) => {
    var name  = person ? person.name : "stranger" ;
    return "hello , " + name ; 
}

console.log(greeting({ name: 'Bom'} ))
console.log(greeting(null));

// ====================
var person = {fname: 'john', lname: 'doe', age:25}
var text = '';
var x;
for(x in person){
    text += person[x];
}
console.log(text)

var text = '';
var person = ['john', 'doe', 25]
person.forEach(element => {
   text +=element
})
console.log(text)

//========== findIndex ==========
var students = [ {firstname: 'bom', lastname: 'thongbai'},
                 {firstname: 'beer', lastname : 'thongbai2'} ];

let indexofkai = students.findIndex(student => student.firstname == 'bom' )
console.log(indexofkai)

let ages = [3, 10, 18, 20];
console.log(ages.findIndex(age => age > 10))

// ========== Map ============
var m = new Map()
m.set('a', 1)
m.set('b', 2)
console.log("Map size : ", m.size);
console.log(m);
m.delete('a')
console.log(m)

m.get('b')
m.has('b')
m.has('a')

var number1 = [45, 4, 9, 16, 25]
var squares = number1.map( n => {
    return n ** 2
});
var squares2 = number1.map( n => {
    return {result : n ** 2}
});
console.log(squares)
console.log(squares2)

var lname = ['thongbai', 'thongbai2'] ; 
var new_students = students.map( (student, index) => 
({ name: student.firstname, place : index + 1 }))

//console.log(new_students)

var new_students = students.map( (student, index) => {
    student = {fullname : student.firstname + " " + student.lastname}
    return student
})
console.log(new_students)

//============= reduce =================
const arr = [1, 2, 3, 4, 5]
const totalValue = arr.reduce((acc, curr) => {
    console.log(acc , " + ", curr , " = " , acc + curr)
    return acc + curr
}, 5)
console.log(totalValue)

